<?php

namespace Eskie\Multichain;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\RequestException;

class JsonRPCClient
{
    protected $host, $username, $password, $version;
    protected $id = 0;
    private $client;

    function __construct($host,$user, $password, $version="2.0")
    {
        $this->host = $host;
        $this->username = $user;
        $this->password = $password;
        $this->version = $version;
        try {
            $this->client=new Client([
                'defaults' => [ 'exceptions' => false],
                'base_uri' => $this->host,
                'auth'  => [$this->username, $this->password],
                'timeout' => 5,
                'http_errors' => false,
                'exceptions' => false,
                'verify' => false
            ]);
        }
        catch(\Exception $e) {
            Log::alert(sprintf());
        }

    }

    function execute($method, $params=array())
    {

        $data = array();
        $data['jsonrpc'] = $this->version;
//        $data['id'] = $this->id++;
        $data['method'] = $method;
        $data['params'] = $params;

        try {
            $res = $this->client->request("POST", '', [
                'json' => $data
            ]);

            $badHeader = [4, 5];
            $rhead = (!empty($res) && is_object($res) && !empty($status = $res->getStatusCode())) ? substr((string) $status, 0,1) : 0;
            $content = (!empty($res->getBody()) && !empty($contents = $res->getBody()->getContents())) ? $contents : 'No Content';

            if (empty($rhead)){
                Log::alert(sprintf('Null Status received: %s ', $status));
                return [];
            }
            if(in_array($rhead, $badHeader))
            {
                Log::alert(sprintf('ReceivedStatus: %s , Message: %s', $status,trim((!$this->isJson($content)) ? json_encode($content) : $content)));
                return [];
            }
            $formatted =  @json_decode($content, true);

            if (!empty($formatted['error'])) {

                Log::alert(sprintf('ReceivedStatus: %s , MessageError: %s', $status,trim(json_encode($formatted['error']))));
                return [];
            } else {
                return (!empty($formatted['result'])) ? $formatted['result'] : [];
            }
        }
        catch (RequestException $e) {
            Log::alert(sprintf('RequestException: %s',$e->getMessage()));
            return [];
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Log::alert(sprintf('ConnectException: %s',$e->getMessage()));
            return [];
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            Log::alert(sprintf('ClientException: %s', $e->getMessage()));
            return [];
        }
        catch (\Exception $e) {
            Log::alert(sprintf('Exception: %s',$e->getMessage()));
            return [];
        }
    }

    function format_response($response)
    {
        return @json_decode($response);
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
