<?php

//declare(strict_types=1);

use Eskie\Multichain\MultichainClient;

if (! function_exists('multichain')) {
    /**
     * Get bitcoind client instance by name.
     *
     * @return \Eskie\Multichain\MultichainClient
     */
    function multichain() : MultichainClient
    {
        return app('multichain');
    }
}
